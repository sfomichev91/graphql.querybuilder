﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphQL.QueryBuilder.Objects;

namespace GraphQL.QueryBuilder
{
    /// <summary>
    /// Билдер GraphQL запросов
    /// </summary>
    public class GraphQLBuilder
    {
        private readonly StringBuilder _builder;

        private readonly List<GraphQLInputItem> _inputData;

        private readonly List<GraphQLOutputItem> _outputData;

        private string _outputMethod;

        #region Public Methods

        /// <summary>
        /// Создает объект с переданным типом запроса
        /// </summary>
        /// <param name="type">Тип запроса (mutation, query)</param>
        public GraphQLBuilder(string type)
        {
            _inputData = new List<GraphQLInputItem>();
            _outputData = new List<GraphQLOutputItem>();

            _builder = new StringBuilder(type);
            Append(" {\n");
        }

        /// <summary>
        /// Устаналивает переданный метод как входной
        /// </summary>
        /// <param name="method">Имя метода</param>
        public void UseMethod(string method)
        {
            Append("  ");
            Append(method);
            Append("  (");
        }

        /// <summary>
        /// Устанавливает входной параметр id и его значение (используется для метода Node)
        /// </summary>
        /// <param name="id">Значение параметра id</param>
        public void UseId(string id)
        {
            Append("id:\"");
            Append(id);
            Append("\") {\n");
        }

        /// <summary>
        /// Устанавливает входной параметр ids и его значение (используется для метода Nodes)
        /// </summary>
        /// <param name="ids">Значения параметра ids</param>
        public void UseIds(string ids)
        {
            Append("ids:\"");
            Append(ids);
            Append("\") {\n");
        }

        /// <summary>
        /// Устаналивает выходной метод
        /// </summary>
        /// <param name="method">Выходной метод, например createdGuideline</param>
        public void UseOutputMethod(string method)
        {
            _outputMethod = method;
        }

        /// <summary>
        /// Добавляет входные данные в запрос
        /// </summary>
        /// <param name="data">Объект с данным</param>
        public void AddInputData(GraphQLInputItem data)
        {
            _inputData.Add(data);
        }

        /// <summary>
        /// Добавляет входные данные в запрос
        /// </summary>
        /// <param name="data">Лист объектов с данным</param>
        public void AddInputData(List<GraphQLInputItem> data)
        {
            _inputData.AddRange(data);
        }

        /// <summary>
        /// Добавляет выходные данные в запрос
        /// </summary>
        /// <param name="data">Объект с данным</param>
        public void AddOutputData(GraphQLOutputItem data)
        {
            _outputData.Add(data);
        }

        /// <summary>
        /// Добавляет выходные данные в запрос
        /// </summary>
        /// <param name="data">Лист объектов с данным</param>
        public void AddOutputData(List<GraphQLOutputItem> data)
        {
            _outputData.AddRange(data);
        }

        /// <summary>
        /// Формирует GraphQL запрос из установленных ранее параметров
        /// </summary>
        /// <returns>Запрос GraphQL</returns>
        public string Build()
        {
            // Рекурсивно добавляем входящие параметры запроса
            RecursiveAppendInput(_inputData);

            // Если у запроса присутствует хотя бы один параметр, значит закрываем блок входных параметров
            if (_inputData.Count > 0)
            {
                Append("){\n");
            }

            // Добавляем выходной параметр, если он задан
            if (!string.IsNullOrWhiteSpace(_outputMethod))
            {
                Append("    ");
                Append(_outputMethod);
                Append(" {\n");
            }

            // Рекурсивно добавляем выходные параметры запроса
            RecursiveAppendOutput(_outputData);

            if (_inputData.Count > 0)
            {
                Append("    }\n");
            }

            if (!string.IsNullOrWhiteSpace(_outputMethod))
            {
                Append("}\n");
            }

            FinalizeFormatting();

            return _builder.ToString();
        }

        #endregion

        #region Private Methods

        private void RecursiveAppendInput(IEnumerable<GraphQLInputItem> items)
        {
            foreach (var item in items)
            {
                // Если у параметра присутствует значение добавляем в запрос как Key:Value
                if (item.Value != null)
                {
                    // Если параметр является вложенным, то добавляем фигурную скобку
                    Append(item.IsNestedObject ? "        { " : "    ");
                    Append(item.Name);
                    Append(": ");

                    var valueType = item.Value.GetType();
                    if (valueType == typeof(bool))
                    {
                        // Если тип текущего параметра bool, то добавляем его без кавычек обязательно в нижнем регистре
                        Append(item.Value.ToString().ToLowerInvariant());
                    }
                    else if (valueType.BaseType == typeof(Enum))
                    {
                        // Если базовый тип текущего параметра Enum, то добавляем его без кавычек как строку
                        Append(item.Value.ToString());
                    }
                    else
                    {
                        // Остальные значения добавляются в кавычках
                        Append("\"");
                        Append(item.Value);
                        Append("\"");
                    }

                    // Если параметр являлся вложенным, закрываем фигурную скобку после значения
                    Append(item.IsNestedObject ? " }\n" : "\n");
                }
                else
                {
                    // Если у параметра отсутствует значение, то добавляем имя параметра и дальше добавляем значения из листа Values
                    Append("    ");
                    Append(item.Name);
                    Append(": ");

                    // Если элемент является массивом добавляем открывающую квадратную скобку в запрос
                    if (item.IsArray)
                    {
                        Append("[\n");
                    }

                    // Если элемент является вложенным добавляем открывающую фигурную скобку в запрос
                    if (item.IsNestedObject)
                    {
                        Append("{\n\t");
                    }

                    RecursiveAppendInput(item.Values);

                    // Закрываем скобки в запрос в зависимости от типа элемента
                    if (item.IsNestedObject)
                    {
                        Append("    }\n");
                    }

                    if (item.IsArray)
                    {
                        Append("    ]\n");
                    }
                }
            }
        }

        private void RecursiveAppendOutput(IEnumerable<GraphQLOutputItem> items)
        {
            foreach (var item in items)
            {
                if (item.Values.Count == 0)
                {
                    Append("      ");
                    Append(item.Name);
                    Append("\n");
                }
                else
                {
                    Append("      ");
                    Append(item.Name);
                    Append(" {\n");

                    RecursiveAppendOutput(item.Values);

                    Append("      }\n");
                }
            }
        }

        private void Append(object obj)
        {
            _builder.Append(obj);
        }

        private void FinalizeFormatting()
        {
            var openedCount = _builder.ToString().Count(e => e.Equals('{'));
            var closedCount = _builder.ToString().Count(e => e.Equals('}'));
            if (openedCount == closedCount)
            {
                return;
            }

            for (int i = 0; i < openedCount - closedCount; i++)
            {
                Append("\n}");
            }
        }

        #endregion
    }
}
