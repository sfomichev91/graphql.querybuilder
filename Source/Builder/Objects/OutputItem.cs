﻿using System.Collections.Generic;

namespace GraphQL.QueryBuilder.Objects
{
    /// <summary>
    /// Объект выходные данных запроса
    /// </summary>
    public class GraphQLOutputItem
    {
        /// <summary>
        /// Имя параметра
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Возможножные значение
        /// </summary>
        public List<GraphQLOutputItem> Values { get; } = new List<GraphQLOutputItem>();

        /// <summary>
        /// Создает объект с заданными именем параметра
        /// </summary>
        /// <param name="name">Имя параметра</param>
        public GraphQLOutputItem(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Создает объект с заданными параметрами
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="values">Возможножные значение, используется, только если параметр Value == null</param>
        public GraphQLOutputItem(string name, List<GraphQLOutputItem> values)
        {
            Name = name;
            Values = values;
        }
    }
}
