﻿using System.Collections.Generic;

namespace GraphQL.QueryBuilder.Objects
{
    /// <summary>
    /// Объект входных данных запроса
    /// </summary>
    public class GraphQLInputItem
    {
        /// <summary>
        /// Имя параметра
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Значение параметра
        /// </summary>
        public object Value { get; }

        /// <summary>
        /// Флаг, указывающий является ли параметр массивом
        /// </summary>
        public bool IsArray { get; }

        /// <summary>
        /// Флаг, указывающий является ли параметр вложенным объектом
        /// </summary>
        public bool IsNestedObject { get; set; }

        /// <summary>
        /// Возможножные значение, используется, только если параметр Value == null
        /// </summary>
        public List<GraphQLInputItem> Values { get; } = new List<GraphQLInputItem>();

        /// <summary>
        /// Создает объект с заданными параметрами
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра</param>
        /// <param name="isInnerObject">Флаг, указывающий является ли параметр вложенным объектом, по умолчанию false</param>
        /// <param name="isArray">Флаг, указывающий является ли параметр массивом, по умолчанию false</param>
        public GraphQLInputItem(string name, object value, bool isInnerObject = false, bool isArray = false)
        {
            Name = name;
            Value = value;
            IsArray = isArray;
            IsNestedObject = isInnerObject;
        }

        /// <summary>
        /// Создает объект с заданными параметрами
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="values">Возможножные значение, используется, только если параметр Value == null</param>
        /// <param name="isInnerObject">Флаг, указывающий является ли параметр вложенным объектом, по умолчанию false</param>
        /// <param name="isArray">Флаг, указывающий является ли параметр массивом, по умолчанию false</param>
        public GraphQLInputItem(string name, List<GraphQLInputItem> values, bool isInnerObject = false, bool isArray = false)
        {
            Name = name;
            Values = values;
            IsArray = isArray;
            IsNestedObject = isInnerObject;
        }
    }
}
