# GraphQL.QueryBuilder
Библиотека для построения GraphQL запросов

# Пример использования
## Пример №1
### Построение запроса на создание рекомендации
```
var builder = new GraphQLBuilder("mutation");

builder.SetMethod("createGuideline");
builder.SetInput();
builder.AddInputData(new List<GraphQlInputItem>
{
	new GraphQlInputItem("title", "Тестовая рекомендация"),
	new GraphQlInputItem("needEquipment", true),
	new GraphQlInputItem("unitId", "e7e2e872-d473-426c-949f-000408d82b6f"),
	new GraphQlInputItem("actions", new List<GraphQlInputItem>
		{
			new GraphQlInputItem("title", "Действие 1", true),
			new GraphQlInputItem("title", "Действие 2", true),
			new GraphQlInputItem("title", "Действие 3", true)
		}
		, false, true),
});

builder.SetOutputMethod("createdGuideline");
builder.AddOutputData(new List<GraphQlOutputItem>
{
	new GraphQlOutputItem("id"),
	new GraphQlOutputItem("needEquipment"),
	new GraphQlOutputItem("title")
});

var query = builder.Build();
```
### Результат:
```
mutation {
  createGuideline(input:{
    title: "Тестовая рекомендация"
    needEquipment: true
    unitId: "e7e2e872-d473-426c-949f-000408d82b6f"
    actions: [
    	{ title: "Действие 1" }
    	{ title: "Действие 2" }
    	{ title: "Действие 3" }
    ]
  })
  {
    createdGuideline {
      id
      needEquipment
      title
    }
  }
}
```
## Пример №2
### Построение запроса на получение информации о юните
```
var builder = new GraphQLBuilder("query");

builder.SetMethod("units");
builder.SetInput();
builder.AddInputData(new List<GraphQlInputItem>
{
	new GraphQlInputItem("filter", new List<GraphQlInputItem>
	{
		new GraphQlInputItem("id", new List<GraphQlInputItem>
		{
			new GraphQlInputItem("eq", "e7e2e872-d473-426c-949f-000408d82b6f")
		},true)
	}, true)
});

builder.AddOutputData(new List<GraphQlOutputItem>
{
	new GraphQlOutputItem("id"),
});

var query = builder.Build();
```
### Результат
```
query {
  units(input:{
    filter: {
      id: {
        eq: "e7e2e872-d473-426c-949f-000408d82b6f"
      }
    }
  }){
    id
  }
}
```
## Пример №3
### Построение запроса на получение информации о текущем пользователе
```
var builder = new GraphQLBuilder("query");

builder.SetOutputMethod("viewer");
builder.AddOutputData(new List<GraphQlOutputItem>
{
	new GraphQlOutputItem("id"),
	new GraphQlOutputItem("userName"),
});

var query = builder.Build();
```
### Результат
```
query {
    viewer {
      id
      userName
  }
}
```
## Пример №4
### Построение запроса на получение информации через метод node
```
var builder = new GraphQLBuilder("query");

builder.SetMethod("node");
builder.SetId("V2VsbDplN2UyZTg3Mi1kNDczLTQyNmMtOTQ5Zi0wMDA0MDhkODJiNmY=");
builder.AddOutputData(new List<GraphQlOutputItem>
{
	new GraphQlOutputItem("id"),
	new GraphQlOutputItem("...on Guideline", new List<GraphQlOutputItem>
	{
		new GraphQlOutputItem("actions", new List<GraphQlOutputItem>
		{
			new GraphQlOutputItem("title")
		})
	})
});

var query = builder.Build();
```
### Результат
```
query {
  node  (id:"V2VsbDplN2UyZTg3Mi1kNDczLTQyNmMtOTQ5Zi0wMDA0MDhkODJiNmY=") {
      id
      ...on Guideline {
      	actions {
      		title
      	}
      }
	}
}
```